package PTA.features.addtocart;

import PTA.steps.serenity.HomePageLoggedSteps;
import PTA.steps.serenity.HomePageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.thucydides.core.annotations.ClearCookiesPolicy.Never;

@RunWith(SerenityRunner.class)
public class AddToCart {

    @Managed(uniqueSession = true, clearCookies=Never)
    public WebDriver webdriver;

    @Steps
    public HomePageSteps user_home;

    @Steps
    public HomePageLoggedSteps user_home_logged;

    @Before
    public void should_login(){
        //login
        user_home.is_the_home_page();
        user_home.login("imci0142@scs.ubbcluj.ro", "imci0142");
    }

    @Test
    public void should_add_to_cart() {
        user_home_logged.search_by("68313_001");
        user_home_logged.add_to_cart();
    }




} 