
@Narrative(
        title = "Add to cart",
        text = {"In order to buy a toy",
                "As a user",
                "I want to be able to add to cart"},
        cardNumber = "#123"
)
package PTA.features.addtocart;

import net.thucydides.core.annotations.Narrative;
