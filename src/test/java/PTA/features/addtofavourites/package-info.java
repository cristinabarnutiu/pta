
@Narrative(
        title = "Add to favourites",
        text = {"In order to make a list",
                "As a user",
                "I want to be able to add to favourites"},
        cardNumber = "#123"
)
package PTA.features.addtofavourites;

import net.thucydides.core.annotations.Narrative;
