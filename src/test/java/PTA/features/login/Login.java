package PTA.features.login;

import PTA.steps.serenity.HomePageLoggedSteps;
import PTA.steps.serenity.HomePageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.thucydides.core.annotations.ClearCookiesPolicy.Never;

@RunWith(SerenityRunner.class)
public class Login {

    @Managed(uniqueSession = true, clearCookies=Never)
    public WebDriver webdriver;

    @Steps
    public HomePageSteps user_home;

    @Steps
    public HomePageLoggedSteps user_home_logged;

    @Before
    public void is_home_page(){ user_home.is_the_home_page(); }

    @Test
    public void should_login() {
        user_home.click_agree();
        user_home.click_login();
        user_home.enter_credentials("imci0142@scs.ubbcluj.ro","imci0142");
        user_home.click_submit();
        user_home_logged.is_contul_meu();
    }

} 