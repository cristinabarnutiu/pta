
@Narrative(
        title = "Login",
        text = {"In order to access my account",
                "As a user",
                "I want to be able to login"},
        cardNumber = "#123"
)
package PTA.features.login;

import net.thucydides.core.annotations.Narrative;