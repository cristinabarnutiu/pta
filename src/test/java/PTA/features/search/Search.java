package PTA.features.search;

import PTA.steps.serenity.HomePageLoggedSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import PTA.steps.serenity.HomePageSteps;

import static net.thucydides.core.annotations.ClearCookiesPolicy.Never;

@RunWith(SerenityRunner.class)
public class Search {

    @Managed(uniqueSession = true, clearCookies=Never)
    public WebDriver webdriver;

    @Steps
    public HomePageSteps user_home;

    @Steps
    public HomePageLoggedSteps user_home_logged;


    @Before
    public void should_login(){
        //login
        user_home.is_the_home_page();
        user_home.login("imci0142@scs.ubbcluj.ro", "imci0142");}


   @Test
    public void should_search_products() {

        //CASE 1: invalid data (should return no result)
       user_home_logged.search_with_no_results("thisproductdoesnotexist");
       //user_home_logged.get_no_result();

       //CASE 2: valid data (should return single result)
        user_home_logged.search_with_single_result("sectie de politie");
        //user_home_logged.get_one_result();

        //CASE 3: valid data (should return one or multiple results)
       user_home_logged.search_by("lego duplo");
    }

//    @Test
//    public void should_add_to_favourites(){
//        user_home_logged.search_by("TOIM4623_001w");
//        user_home_logged.get_one_result();
//        user_home_logged.add_to_wishlist();
// }
} 