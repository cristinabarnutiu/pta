package PTA.features.search;


import PTA.steps.serenity.HomePageLoggedSteps;
import PTA.steps.serenity.HomePageSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.thucydides.core.annotations.ClearCookiesPolicy.Never;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("D:\\POSTUNIVERSITAR\\10-VVTA\\PTAPROJECT\\src\\test\\resources\\produse.csv")


public class SearchCSV {
    @Managed(uniqueSession = true, clearCookies=Never)
    public WebDriver webdriver;

    @Steps
    public HomePageSteps user_home;

    @Steps
    public HomePageLoggedSteps user_home_logged;

    public String cod;
    public boolean stoc;

    @Test
    public void search_products_from_csv() {
        user_home.is_the_home_page();
        user_home.login("imci0142@scs.ubbcluj.ro","imci0142");
        //user_home_logged.search_by(cod);
        user_home_logged.search_with_single_result(cod);
    }
}



