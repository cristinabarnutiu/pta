
@Narrative(
        title = "Search for products",
        text = {"In order to find toys I want to buy",
                "As a user",
                "I want to be able to search for products"},
        cardNumber = "#123"
)
package PTA.features.search;

import net.thucydides.core.annotations.Narrative;