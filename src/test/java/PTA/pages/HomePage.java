package PTA.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://noriel.ro/")
public class HomePage extends PageObject {

    @FindBy (className ="daAgree")
    private WebElementFacade agreeButton;

    @FindBy(id = "authorization-trigger")
    private WebElementFacade loginButton;

    @FindBy (id = "login-form-header")
    private WebElementFacade loginForm;

    @FindBy (id = "email")
    private WebElementFacade emailTextBox;

    @FindBy (id = "pass")
    private WebElementFacade passwordTextBox;

    @FindBy (className = "login-mini-submit")
    private WebElementFacade submitButton;

    public void click_agreeButton() {
        agreeButton.click();
    }
    public void click_loginButton() {
        loginButton.click();
    }

    public void enter_email(String email) {
        emailTextBox.type(email);
    }

    public void enter_password (String password) {
        passwordTextBox.type(password);
    }

    public void click_submit () {
        submitButton.click();
    }



//    public void lookup_terms() {
//        lookupButton.click();
//    }
//
//    public List<String> getDefinitions() {
//        WebElementFacade definitionList = find(By.tagName("ol"));
//        return definitionList.findElements(By.tagName("li")).stream()
//                .map( element -> element.getText() )
//                .collect(Collectors.toList());
//    }
}