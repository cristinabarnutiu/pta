package PTA.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePageLogged extends PageObject {

    //@FindBy(xpath = "/html/body/div[4]/header/div/form/input")
    //private WebElementFacade searchTextBox;

    @FindBy(id = "search")
    private WebElementFacade searchTextBox;

    @FindBy (id = "product-addtocart-button")
    public WebElementFacade addToCartButton;

    @FindBy (className = "tocart")
    public WebElementFacade toCartButton;

    public boolean is_addtocart_or_tocart_visible(){
        if (addToCartButton.isPresent() || toCartButton.isPresent())
            return true;
        else return false;
    }

    public void enter_search_word(String key) {
        searchTextBox.typeAndEnter(key);
    }

    public WebElementFacade getAddToCartButton(){return addToCartButton;}

    public void click_addToCartButton() {
        addToCartButton.click();
    }

    public WebElementFacade getToCart() {
        return toCartButton;
    }

    @FindBy (css = "#product_addtocart_form > div.product-container-right > a.second-action.towishlist")
    private WebElementFacade addToWishlistButton;

    @FindBy (className = "message-success success message")
    public WebElementFacade successMessage;
    public boolean isSuccessMessageVisisble(){return successMessage.isVisible();}

    @FindBy (xpath = "//*[@id=\"minicart-wrapper\"]/span")
    public WebElementFacade cartCount;
    public String get_cartCount(){return cartCount.getTextValue();}

    @FindBy (className = "title-my-account")
    public WebElementFacade title;
    public String get_title (){return title.getTextContent();}

    public void click_add_to_wishlist(){ addToWishlistButton.click(); }
    public boolean is_wishlist_success_message(){ return successMessage.isVisible();}

    @FindBy (id = "header-panel-buton  logged-in")
    private WebElementFacade contulMeu;
    public WebElementFacade getContulMeu() { return contulMeu; }

    @FindBy (id = "page-title-heading")
    public WebElementFacade heading;
    public String getHeading() { return heading.getText(); }
    public boolean isHeadingPresent(){return heading.isVisible();}

    //public boolean is_addToCart_success_message() {return successMessage.isVisible();
    //}

    public String get_addToCart_success_message() {return successMessage.getText();
    }
//    public List<String> get_results() {
//        WebElementFacade results = find(By.tagName("span"));
//        return definitionList.findElements(By.tagName("li")).stream()
//                .map( element -> element.getText() )
//                .collect(Collectors.toList());
//    }


}