package PTA.steps.serenity;

import PTA.pages.HomePage;
import PTA.pages.HomePageLogged;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.sql.SQLOutput;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class HomePageLoggedSteps {

    HomePageLogged homePageLogged;

    @Step
    public void search_by(String key) {
        homePageLogged.enter_search_word(key);
    }

    @Step
    public void search_with_single_result (String key){
        homePageLogged.enter_search_word(key);
        assertThat(homePageLogged.getHeading(), containsString(key));
    }

    @Step
    public void search_with_no_results (String key){
        homePageLogged.enter_search_word(key);
        assertFalse(homePageLogged.isHeadingPresent());
    }

    public void search_with_multiple_results (String key) {
        homePageLogged.enter_search_word(key);
        assertTrue(homePageLogged.addToCartButton.isPresent() || homePageLogged.toCartButton.isPresent());
    }

    @Step
    public void is_in_stock(String key, boolean stoc) {
        homePageLogged.enter_search_word(key);
//        boolean instoc = homePageLogged.is_addtocart_or_tocart_visible();
//        System.out.println(instoc);

        boolean instoc = homePageLogged.addToCartButton.isPresent() || homePageLogged.toCartButton.isPresent();
        System.out.println("instoc: "+ instoc);
//      System.out.println(homePageLogged.addToCartButton.getValue());
        System.out.println("getText: " + homePageLogged.addToCartButton.getText());
        System.out.println("getTextContent: " + homePageLogged.addToCartButton.getTextContent());
        System.out.println("isVisible: " + homePageLogged.addToCartButton.isVisible());
        System.out.println("isPresent: " + homePageLogged.addToCartButton.isPresent());
        System.out.println("isDisplayed: " + homePageLogged.addToCartButton.isDisplayed());
        System.out.println("isCurrentlyVisible: " + homePageLogged.addToCartButton.isCurrentlyVisible());
        System.out.println(stoc);
        System.out.println(instoc);
        assertEquals(stoc,instoc);
    }

    @Step
    public void add_to_wishlist(){
        homePageLogged.click_add_to_wishlist();
        System.out.println("The current page is: " + homePageLogged.get_title());
        assertEquals (homePageLogged.get_title(), "Favorite");
    }

    @Step
    public void is_contul_meu(){
        System.out.println(homePageLogged.getContulMeu());
        assertThat(homePageLogged.getContulMeu(), notNullValue()); }

        @Step
        public void add_to_cart(){
        int initial_cart = Integer.parseInt(homePageLogged.get_cartCount());
            System.out.println("cart count before add_to_cart:" + initial_cart);
        homePageLogged.click_addToCartButton();
        int after_cart = Integer.parseInt(homePageLogged.get_cartCount());
            System.out.println("cart count after add_to_cart:" + after_cart);
        assertNotEquals(initial_cart,after_cart);
    }
}