package PTA.steps.serenity;

import PTA.pages.HomePage;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class HomePageSteps {

    HomePage homePage;

    @Step
    public void is_the_home_page() {
        homePage.open();
    }

    @Step
    public void click_agree() {
        homePage.click_agreeButton();
    }

    @Step
    public void click_login() {
        homePage.click_loginButton();
    }

    @Step
    public void enter_credentials(String email, String password) {
        homePage.enter_email(email);
        homePage.enter_password(password);
    }

    @Step
    public void click_submit() {
        homePage.click_submit();
    }

    @Step
    public void login(String email, String password) {
        click_agree();
        click_login();
        enter_credentials(email, password);
        click_submit();
    }


}